<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBukuBestSellerIdToPenulisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('penulis', function (Blueprint $table) {
            $table->unsignedBigInteger('buku_best_seller_id')->nullable();

            $table->foreign('buku_best_seller_id')->references('id')->on('buku');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('penulis', function (Blueprint $table) {
            $table->dropColumn('buku_best_seller_id');
            $table->dropForeign('posts_user_id_foreign');
        });
    }
}
