<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPenerbitIdAndCategoryIdToBukuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('buku', function (Blueprint $table) {
            $table->unsignedBigInteger('penerbit_id')->nullable();
            $table->unsignedBigInteger('kategori_id')->nullable();

            $table->foreign('penerbit_id')->references('id')->on('penerbit');
            $table->foreign('kategori_id')->references('id')->on('kategori');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('buku', function (Blueprint $table) {
            $table->dropColumn(['penerbit_id', 'kategori_id']);
            $table->dropForeign(['penerbit_id', 'kategori_id']);
        });
    }
}
