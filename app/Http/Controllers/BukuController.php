<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class BukuController extends Controller
{
    public function index()
    {
        $data = DB::table('buku')->get();
        return view('buku.index', compact('data'));
    }

    public function create()
    {
        return view('buku.create');
    }

    public function store(Request $request)
    {
        DB::table('buku')->insert([
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi,
            'jumlah_halaman' => $request->jumlah_halaman,
            'tahun_terbit' => $request->tahun_terbit,
            'genre' => $request->genre
        ]);

        return redirect('/buku');
    }

    public function show($id)
    {
        $buku = DB::table('buku')->where('id', $id)->first();

        return view('buku.show', compact('buku'));
    }

    public function edit($id)
    {
        $buku = DB::table('buku')->where('id', $id)->first();

        return view('buku.edit', compact('buku'));
    }


    public function update(Request $request, $id)
    {
        DB::table('buku')
                ->where('id', $id)
                ->update([
                    'judul' => $request->judul,
                    'deskripsi' => $request->deskripsi,
                    'jumlah_halaman' => $request->jumlah_halaman,
                    'tahun_terbit' => $request->tahun_terbit,
                    'genre' => $request->genre
                ]);

        return redirect('/buku');
    }

    public function destroy($id)
    {
        DB::table('buku')->where('id', $id)->delete();
        return redirect('/buku');
    }
}
