<?php

Route::get('/', function () {
    return view('erd');
});

Route::get('/buku', 'BukuController@index');
Route::get('/buku/create', 'BukuController@create');
Route::post('/buku', 'BukuController@store');
Route::get('/buku/{id}', 'BukuController@show');
Route::any('/buku/{id}/edit', 'BukuController@edit');
Route::put('/buku/{id}', 'BukuController@update');
Route::delete('/buku/{id}', 'BukuController@destroy');
