@extends('layouts.master')

@section('judul_soal', 'Tambah Data Buku')

@section('content')

<form action="/buku" method="POST">
    @csrf
    <div class="form-group">
        <label for="judul">Judul Buku</label>
        <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul Buku"
            value="{{ old('judul') }}">
    </div>
    <div class="form-group">
        <label for="deskripsi">Deskripsi Buku</label>
        <textarea class="form-control" name="deskripsi" id="deskripsi"
            placeholder="Deskripsi buku">{{ old('deskripsi') }}</textarea>

    </div>
    <div class="form-group">
        <label for="jumlah_halaman">Jumlah Halaman Buku</label>
        <input type="number" class="form-control" id="jumlah_halaman" name="jumlah_halaman"
            placeholder="Jumlah Halaman Buku" value="{{ old('jumlah_halaman') }}">
    </div>
    <div class="form-group">
        <label for="tahun_terbit">Tahun Terbit Buku</label>
        <input type="text" class="form-control" id="tahun_terbit" name="tahun_terbit" placeholder="Tahun Terbit Buku"
            value="{{ old('tahun_terbit') }}">
    </div>
    <div class="form-group">
        <label for="genre">Genre Buku</label>
        <input type="text" class="form-control" id="genre" name="genre" placeholder="Genre Buku"
            value="{{ old('genre') }}">
    </div>
    <div class="form-group">
        <input type="submit" value="Simpan" class="btn btn-primary">
    </div>
</form>

@endsection
