@extends('layouts.master')

@section('judul_soal', 'Memasangkan Sweat Alert dan Daftar Buku')

@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">

    <div class="mb-4 shadow card">
        <div class="py-3 card-header">
            <a href="/buku/create" class="btn btn-primary">Tambah Buku</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Judul</th>
                            <th>Deskripsi</th>
                            <th>Jumlah Halaman</th>
                            <th>Tahun Terbit</th>
                            <th>Genre</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Judul</th>
                            <th>Deskripsi</th>
                            <th>Jumlah Halaman</th>
                            <th>Tahun Terbit</th>
                            <th>Genre</th>
                            <th>Actions</th>
                        </tr>
                    </tfoot>
                    <tbody>

                        @foreach($data as $buku)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $buku->judul }}</td>
                            <td>{{ $buku->deskripsi }}</td>
                            <td>{{ $buku->jumlah_halaman }} </td>
                            <td>{{ $buku->tahun_terbit }} </td>
                            <td>{{ $buku->genre}}</td>
                            <td style="display: flex">
                                <a href="/buku/{{ $buku->id }}" class="mr-1 text-white btn btn-primary btn-xs">
                                    Show
                                </a>
                                <a href="/buku/{{ $buku->id }}/edit" class="mr-1 btn btn-warning btn-xs">
                                    Edit
                                </a>
                                <form action="/buku/{{ $buku->id }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="text-white btn btn-danger btn-xs" value="Delete">

                                </form>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->


@endsection

@push('scripts')

<script>
    Swal.fire({
        title: "Berhasil!",
        text: "Memasangkan script sweet alert",
        icon: "success",
        confirmButtonText: "Cool",
    });
</script>

@endpush
