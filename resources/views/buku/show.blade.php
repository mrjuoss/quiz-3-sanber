@extends('layouts.master')

@section('judul_soal', 'Menampilkan Data Buku')

@section('content')

<div class="form-group">
    <label for="judul">Judul Buku</label>
    <input type="text" class="form-control" id="judul" name="judul" value="{{ $buku->judul }}" disabled>
</div>
<div class="form-group">
    <label for="deskripsi">Deskripsi Buku</label>
    <textarea class="form-control" name="deskripsi" id="deskripsi" disabled>{{ $buku->deskripsi }}</textarea>

</div>
<div class="form-group">
    <label for="jumlah_halaman">Jumlah Halaman Buku</label>
    <input type="number" class="form-control" id="jumlah_halaman" name="jumlah_halaman"
        value="{{ $buku->jumlah_halaman }}" disabled>
</div>
<div class="form-group">
    <label for="tahun_terbit">Tahun Terbit Buku</label>
    <input type="text" class="form-control" id="tahun_terbit" name="tahun_terbit" value="{{ $buku->tahun_terbit }}"
        disabled>
</div>
<div class="form-group">
    <label for="genre">Genre Buku</label>
    <input type="text" class="form-control" id="genre" name="genre" value="{{ $buku->genre }}" disabled>
</div>

@endsection
