@extends('layouts.master')

@section('judul_soal', 'Edit Data Buku')

@section('content')

<form action="/buku/{{ $buku->id }}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="judul">Judul Buku</label>
        <input type="text" class="form-control" id="judul" name="judul" value="{{ $buku->judul }}">
    </div>
    <div class="form-group">
        <label for="deskripsi">Deskripsi Buku</label>
        <textarea class="form-control" name="deskripsi" id="deskripsi"
            placeholder="Deskripsi buku">{{ $buku->deskripsi }}</textarea>

    </div>
    <div class="form-group">
        <label for="jumlah_halaman">Jumlah Halaman Buku</label>
        <input type="number" class="form-control" id="jumlah_halaman" name="jumlah_halaman"
            placeholder="Jumlah Halaman Buku" value="{{ $buku->jumlah_halaman }}">
    </div>
    <div class="form-group">
        <label for="tahun_terbit">Tahun Terbit Buku</label>
        <input type="text" class="form-control" id="tahun_terbit" name="tahun_terbit" value="{{ $buku->tahun_terbit }}">
    </div>
    <div class="form-group">
        <label for="genre">Genre Buku</label>
        <input type="text" class="form-control" id="genre" name="genre" value="{{ $buku->genre }}">
    </div>
    <div class="form-group">
        <input type="submit" value="Simpan" class="btn btn-primary">
    </div>
</form>

@endsection
